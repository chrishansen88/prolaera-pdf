import "babel-polyfill";
import { Document, Image, Page, StyleSheet, View } from "@react-pdf/core";
import ReactPDF from "@react-pdf/node";
import "babel-core/register";
import "babel-polyfill";
import React from "react";
import Address from "./components/address";
import CertInfo from "./components/certInfo";
import Logo from "./components/logo";
import RegistrationNumbers from "./components/registrationNumbers";
import Signature from "./components/signature";
import SponsorStatement from "./components/sponsorStatement";

const styles = StyleSheet.create({
  page: { display: "flex" },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: "-1"
  },
  logo: {},
  address: { marginRight: "25px" },
  topRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "25px",
    marginLeft: "25px",
    marginRight: "25px"
  },
  subRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: "45px",
    marginRight: "45px"
  },
  certInfo: {
    marginTop: "10px",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  sponsorStatement: {
    marginTop: "35px",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  }
});

//TODO:

class Certificate extends React.Component {
  render() {
    const { cert } = this.props;
    return (
      <Document author="Prolaera" title="Certificate">
        <Page size="A4" orientation="landscape" style={styles.page}>
          <Image
            src="https://assets.prolaera.com/pdf-background.jpg"
            style={styles.background}
          />
          <View style={styles.topRow}>
            <Logo {...cert} />
            <Address {...cert} style={styles.address} />
          </View>
          <View style={styles.certInfo}>
            <CertInfo {...cert} />
          </View>
          <View style={styles.subRow}>
            <Signature {...cert} />
            <RegistrationNumbers {...cert} />
          </View>
          <View style={styles.sponsorStatement}>
            <SponsorStatement {...cert} />
          </View>
        </Page>
      </Document>
    );
  }
}

export { Certificate };

export default async function(cert) {
  const stream = await ReactPDF.renderToStream(<Certificate cert={cert} />);
  return stream;
}
