import streamCertificate from ".";
import fs from "fs";

const sponsors = {
  active: false,
  addressOne: null,
  addressTwo: null,
  archived: true,
  city: null,
  company_id: 1,
  logoName: null,
  logoUrl: null,
  name: "New Sponsor - 5/11/2018",
  signature: {
    imageUrl: null,
    name: null,
    title: null
  },
  signatureName: {},
  signatureUrl: null,
  sponsor_id: "0b6bc2a6-7f90-4372-85b0-b067c98f1d7d",
  sponsors: {
    allOthers: null,
    illinois: "234234",
    irs: null,
    name: null,
    nationalRegistry: "234234",
    newJersey: null,
    newYork: null,
    pennsylvania: null,
    qAS: null,
    texas: null
  },
  sponsors_types: {
    allOthers: false,
    illinois: false,
    irs: false,
    name: false,
    nationalRegistry: false,
    newJersey: false,
    newYork: false,
    pennsylvania: false,
    qAS: false,
    texas: false
  },
  sponsorStatement:
    "In accordance with the standards of the National Registry of CPE Sponsors and as set forth in Circular 230 Section 10.6, credits have been granted based on a 50-minute hour. State boards of accountancy, state bars, and other state regulatory bodies have final authority on the acceptance of courses for continuing education credit. Please check with your state board regarding applicability of this course towards your continuing education requirement.",
  state: null,
  style: null,
  zip: null
};

const cert = {
  cert:
    "Meeting SEC Disclosure Requirements: Compensation Discussion & Analysis",
  date: "2017-06-26T08:00:00.000Z",
  first: "Hugh",
  last: "Mann",
  delivery_date: "2018-07-18T22:00:00.000Z",
  delivery_method: 5,
  hours: [
    {
      subject_area: "Imported",
      credits: 3
    },
    {
      subject_area: "Hog-Tying",
      credits: 2
    }
  ],
  sponsors
};

describe("ReactPDF Render", async () => {
  it("should render a PDF file for testing purposes", async () => {
    jest.setTimeout(30000);
    try {
      const rstream = await streamCertificate(cert);
      const wstream = fs.createWriteStream(`${__dirname}/output.pdf`);
      rstream.pipe(wstream);
      expect(rstream).toBeDefined();
    } catch (error) {
      throw error;
    }
  });
});

// var rstream = fs.createReadStream('existingFile');
